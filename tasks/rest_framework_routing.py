from rest_framework import routers
from tasks.views import TasksView, TaskAssignView


router = routers.DefaultRouter()
router.register(r'tasks', TasksView)
router.register(r'taskassign', TaskAssignView)
