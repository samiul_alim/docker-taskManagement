from rest_framework import serializers
from tasks.models import Tasks, TaskTypes, TaskAssign


class TasksSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Tasks
        fields = ('url', 'name')


class TaskAssignSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = TaskAssign
        fields = ('url', 'task', 'developer')
