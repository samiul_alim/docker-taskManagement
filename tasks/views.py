from django.shortcuts import render, HttpResponse
from tasks.models import Tasks, TaskAssign, TaskTypes
from django.core.serializers import serialize
import json
from rest_framework import viewsets
from tasks.serializers import TasksSerializer, TaskAssignSerializer


class TaskView:

    def task_index(request):
        types = TaskTypes.objects.all()
        return render(request, 'tasks/task_types.html', dict(types=types))

    def get_tasks_by_type(request):
        response = dict(success=False, tasks=[])
        try:
            type_id = request.GET.get('type')
            if type_id:
                task_assign_list = []
                if type_id == 'all':
                    tasks = TaskAssign.objects.all()
                    for task in tasks:
                        task_assign_list.append(dict(
                            task=task.task.name,
                            type=task.task.task_type.name,
                            developer=task.developer.get_full_name(),
                            status=task.status
                        ))
                    response['success'] = True
                    response['tasks'] = task_assign_list
                elif type_id.isdigit():
                    tasks = TaskAssign.objects.filter(task__task_type_id=type_id)
                    for task in tasks:
                        task_assign_list.append(dict(
                            task=task.task.name,
                            type=task.task.task_type.name,
                            developer=task.developer.get_full_name(),
                            status=task.status
                        ))
                    response['success'] = True
                    response['tasks'] = task_assign_list
        except Exception as ex:
            print(ex)
        return HttpResponse(json.dumps(response), content_type='application/json')


class TasksView(viewsets.ModelViewSet):
    queryset = Tasks.objects.all()
    serializer_class = TasksSerializer


class TaskAssignView(viewsets.ModelViewSet):
    queryset = TaskAssign.objects.all()
    serializer_class = TaskAssignSerializer
