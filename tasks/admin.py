from django.contrib import admin
from tasks.models import TaskTypes, Tasks, TaskAssign, Developers


@admin.register(Developers)
class DevelopersAdmin(admin.ModelAdmin):
    fields = ('username', 'password', 'first_name', 'last_name')
    list_display = ('id', 'username', 'first_name', 'last_name')


@admin.register(TaskTypes)
class TaskTypesAdmin(admin.ModelAdmin):
    fields = ('name',)
    list_display = ('id', 'name')


@admin.register(Tasks)
class TasksAdmin(admin.ModelAdmin):
    fields = ('name', 'task_type')
    list_display = ('id', 'name', 'task_type')


@admin.register(TaskAssign)
class TaskAssignAdmin(admin.ModelAdmin):
    fields = ('developer', 'task', 'status')
    list_display = ('id', 'task', 'status', 'developer', 'assign_date')


