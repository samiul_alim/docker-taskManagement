$(function () {
    function get_data(type){
        $.ajax({
            url: '/tasks',
            type: "GET",
            data: {
                type: type
            },
            success: function (result) {
                $('.table_tbody').empty()
                if(result.success){
                    var tasks = result.tasks;
                    for(var i=0; i<tasks.length; i++) {
                        var t = tasks[i];
                        var tr = '<tr><td>'+t.task+'</td> <td>'+t.type+'</td> <td>'+t.developer+'</td> <td>'+t.status+'</td> </tr>';
                        $('.table_tbody').append(tr);
                    }
                }else {
                    alert('something went wrong!')
                }
            }
        });
    }

    $(".type-selection").change(function () {
        var type = $(this).val();
        get_data(type);
    })

    get_data('all');
});