from django.urls import path, include
from tasks.views import TaskView


urlpatterns = [
    path('', TaskView.task_index, name="task-index"),
    path('tasks', TaskView.get_tasks_by_type, name="tasks-by-type")
]