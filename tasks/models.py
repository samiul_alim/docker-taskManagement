from django.db import models
from django.contrib.auth.models import User
# from django.utils.translation import gettext_lazy as _


class Developers(User):

    # first_name = models.CharField(_('first name'), max_length=30, blank=True)
    # last_name = models.CharField(_('last name'), max_length=150, blank=True)

    class Meta:
        db_table = 'developers'


class TaskTypes(models.Model):
    name = models.CharField(max_length=64)

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'task_types'


class Tasks(models.Model):
    name = models.CharField(max_length=64)
    task_type = models.ForeignKey(TaskTypes, on_delete=models.PROTECT, null=True)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'tasks'


class TaskAssign(models.Model):
    developer = models.ForeignKey(Developers, on_delete=models.CASCADE)
    task = models.ForeignKey(Tasks, on_delete=models.PROTECT)
    status = models.CharField(
        max_length=32,
        default='unassigned',
        choices=(('unassigned', 'Unassigned'), ('ongoing', 'Ongoing'), ('completed', 'Completed'))
    )
    assign_date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return "{} assign to {}".format(self.task.name, self.developer.get_full_name())


    class Meta:
        db_table = 'task_assign'
