FROM python:3.6.5-alpine3.7
ENV PYTHONUNBUFFERED 1

RUN apk add --update \
    bash \
    gcc \
    musl-dev \
    mariadb-dev \
  && rm -rf /var/cache/apk/*

RUN mkdir /app
WORKDIR /app
COPY . /app
RUN pip install -r /app/requirement.txt

COPY entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh