Django==2.0.6
#psycopg2==2.7.4
mysqlclient==1.3.6
djangorestframework==3.8.2
Markdown==2.6.11
pytz==2018.4
